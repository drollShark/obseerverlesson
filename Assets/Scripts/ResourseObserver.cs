using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourseObserver : MonoBehaviour
{
    private Text resourseCountUI;
    private int resourseCount = 0;

    [SerializeField]
    private ResType resourseName;

    private void OnEnable()
    {
        ResourceBank.onResChanges += ResourceBank_onResChanges;
    }

    private void OnDisable()
    {
        ResourceBank.onResChanges -= ResourceBank_onResChanges;
    }

    private void ResourceBank_onResChanges(ResType resType, int value)
    {
        if(resType == resourseName)
        {
            resourseCountUI.text = $"{ResourceBank.resourceBankInstance.GetResourse(resType)}";
        }
    }

    private void Start()
    {
        resourseCountUI = GetComponent<Text>();
        resourseCount = ResourceBank.resourceBankInstance.GetResourse(resourseName);
        resourseCountUI.text = $"{resourseCount}";
    }

}
