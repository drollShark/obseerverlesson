using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ResType
{
    Worker,
    Wood,
    House,
    Money
};

public class ResourceBank : MonoBehaviour
{
    public static ResourceBank resourceBankInstance;

    [SerializeField]
    private Dictionary <ResType, int> resourse = new Dictionary<ResType, int>();

    public static event OnResChanges onResChanges;
    public delegate void OnResChanges(ResType resType, int value);


    private void Awake()
    {
        if (resourceBankInstance == null)
        {
            resourceBankInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        resourse.Add(ResType.Worker, 2);
        resourse.Add(ResType.Wood, 0);
        resourse.Add(ResType.House, 0);
        resourse.Add(ResType.Money, 0);
    }

    private void Start()
    {
        
        StartCoroutine(woodSpawn());
    }


    public void ChangeResourse(ResType resType,int val)
    {
        resourse[resType] = val;
        onResChanges?.Invoke(resType,val);
    }

    public int GetResourse(ResType res)
    {
        return resourse[res];
    }

    IEnumerator woodSpawn()
    {
        while (true)
        {
            int workerCount = GetResourse(ResType.Worker);
            int woodCount = GetResourse(ResType.Wood);
            woodCount += workerCount;
            ChangeResourse(ResType.Wood, woodCount);
            yield return new WaitForSeconds(0.5f);
        }
        
    }
}
