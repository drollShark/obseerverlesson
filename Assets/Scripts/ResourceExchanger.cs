using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceExchanger : MonoBehaviour
{
    //Some variables for Catching bugs 
    private int worker, wood, house, money;

    ResourceBank resourceBank;

    private void OnEnable()
    {
        ResourceBank.onResChanges += ResourceBank_onResChanges;
    }

    private void OnDisable()
    {
        ResourceBank.onResChanges -= ResourceBank_onResChanges;
    }

    private void ResourceBank_onResChanges(ResType resType, int value)
    {
        int count = ResourceBank.resourceBankInstance.GetResourse(resType);
               
        switch (resType)
        {
            case ResType.Wood:
                {
                    wood = count;
                    if (wood >= 10)
                    {
                        wood -= 10;
                        house++;

                        ResourceBank.resourceBankInstance.ChangeResourse(ResType.Wood, wood);
                        ResourceBank.resourceBankInstance.ChangeResourse(ResType.House, house);
                    }
                    break;
                }
            case ResType.House:
                {
                    house = count;
                    if(house >= 5)
                    {
                        int num = Random.Range(1, 5);
                        house -= num;

                        money = ResourceBank.resourceBankInstance.GetResourse(ResType.Money);
                        money += num * 100;

                        ResourceBank.resourceBankInstance.ChangeResourse(ResType.House, house);
                        ResourceBank.resourceBankInstance.ChangeResourse(ResType.Money, money);

                    }
                    break;
                }
            case ResType.Money:
                {
                    money = count;
                    if(money >= 1000)
                    {
                        int num = Random.Range(1, 4);

                        money = ResourceBank.resourceBankInstance.GetResourse(ResType.Money);
                        money -= num * 250;

                        worker = ResourceBank.resourceBankInstance.GetResourse(ResType.Worker);
                        worker += num;

                        ResourceBank.resourceBankInstance.ChangeResourse(ResType.Money, money);
                        ResourceBank.resourceBankInstance.ChangeResourse(ResType.Worker, worker);
                    }
                    break;
                }
        }

    }

   
}
